import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('lab', {path: '/lab'});
  this.route('login', {path: '/'});
  this.route('carrot');
  this.route('meals', function() {
    this.route('meal-detail');
    this.route('products');
  });
  this.route('meal', function() {
    this.route('meal-detail', {path: '/:id'});
  });
});

export default Router;
