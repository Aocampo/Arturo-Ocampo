import Controller from '@ember/controller';

export default Controller.extend({
  cat: 'pelusa',
    weekDays: [{
        day: 'Monday',
        number: 1,
        'mi propiedad': 'hola'
      },
      {
        day: 'Tuesday',
        number: 2,
        'mi propiedad': 'hola'
      },
      {
        day: 'Wednesday',
        number: 3,
        'mi propiedad': 'hola'
      },
      {
        day: 'Thursday',
        number: 4,
        'mi propiedad': 'hola'
      },
      {
        day: 'Friday',
        number: 5,
        'mi propiedad': 'hola'
      },
      {
        day: 'Saturday',
        number: 6,
        'mi propiedad': 'hola'
      },
      {
        day: 'Sunday',
        number: 7,
        'mi propiedad': 'hola'
      },
    ],
    tweets: [],

    actions: {
      doSomething() {
        this.set('cat', "perro");
        let cat = this.get('cat');

        //console.log(`The name is ${cat}`)

        alert('ALERT');
      },
      myLuckyDay() {
        let weekDays = this.get('weekDays')

        var n = Math.floor(Math.random() * weekDays.length);
        this.set('luckyDay', weekDays.objectAt(n)['day'])
        //this.set('luckyDay', weekDays.objectAt(n).day)
      },
      addDay(){
        let newDay = this.get('newDay');
        let weekDays = this.get('weekDays')
        weekDays.pushObject({
          day: newDay,
          //number: Math.floor(Math.random()*100),
          number:weekDays.length +1,
          'mi propiedad': 'hola'
        });
      },
      addTweet(){
        let newTweet = this.get('newTweet')
        let tweets  = this.get('tweets')

        tweets.pushObject({
          user: this.set('user'),
          description: newTweet
        })
        this.set('newTweet', '')

      },
      logout(){
        this.transitionToRoute('login')
      },

      deleteTweet(tweet){
        var tweets = this.get('tweets');
        let result = confirm('Realmente deseas borrar este tweet: ' + tweet.description);
        if (result){
          tweets.removeObject(tweet);
        }
      },
        //alert(tweet.description);

    }
  });
