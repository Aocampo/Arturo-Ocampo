import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
		createProduct(){
			this.store.createRecord('product', {
				name: this.get('name'),
				calories: this.get('calorias')
			}).save().then(()=>{
				this.set('name', '')
				this.set('calories', '')
				alert('El producto fue guardado.')
			})
		},
		deleteProduct(product){
			product.destroyRecord().then(()=>{
				alert("El producto ha sido eliminado.")
			})
		},
		updateProduct(product){
			product.save().then(()=>{
				alert("El producto ha sido actualizado.")
			})
		}
	}

});
