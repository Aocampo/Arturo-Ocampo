import Controller from '@ember/controller';

export default Controller.extend({
  //aqui van las acciones de index.hbs
  actions:{
    SendMeal(){
      var meals = this.get('meals');
      //var name = {name: meals}
      var meals2 = this.store.createRecord('meal', {name: meals});
      var resultado = confirm("¿Estas seguro que quieres añadir?");
      //this.set('isSaving', true);
      if(resultado === true)
      {
        console.log("verificando");
        meals2.save().then(()=> {
          console.log("Si funcionó");

      }).catch((e)=> {
        console.log("No funcionó" + e);

      }).finally(()=> {
        //this.set('isSaving', false);
      })
    }
    },
    editMeal(meal) {
			var resultado = confirm("Confirma si quieres editar");
			if (resultado) {
				meal.save();
			}

		},

		deleteMeal(meal) {
			var resultado = confirm("Confirma si quieres eliminar");
			if (resultado) {
				meal.destroyRecord();
			}

		},
    UselessAction(name, text){
      alert('Just an alert!' + name )
    }
  }
});
