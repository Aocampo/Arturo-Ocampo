import Controller from '@ember/controller';

export default Controller.extend({
	actions: {
		editMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres editar?");
			if (resultado) {
				meal.save();
			}

		},

		deleteMeal(meal) {
			var resultado = confirm("¿Estás seguro que quieres eliminar?");
			if (resultado) {
				meal.destroyRecord().then(() => {
					this.transitionToRoute('meals')
				}).catch((e) => {
					console.log(e)
				})
			}

		}
	}
});
