import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
    EditarMeal(meals)
    {
      //alert(meals.get("name")
      var resultado = confirm("¿Estas seguro que quieres editar?")
      if(resultado = true)
      {
        meals.save();
      }
    },

    EliminarMeal(meals){
      var resultado = confirm("¿Estas seguro que quieres eliminar?")
      if(resultado = true)
      {
        meals.destroyRecord();
        this.transitionToRoute("meals");
      }
    }
  }
});
