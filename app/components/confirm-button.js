import Component from '@ember/component';

export default Component.extend({
	isConfirming: false,
	actions: {
		triggerAction(text){
			this.set('isConfirming', true)
			if(confirm("¿Quieres eliminar " + text + "?")){
				this.onCall()
					.then(()=>{
						console.log('ya acabaste')
						this.set('isConfirming', false)
					})
					.catch(()=>{

					})
			}
		}
	}
});
